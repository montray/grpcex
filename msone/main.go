package main

import (
	"context"
	"fmt"
	"log"
	"net"

	ms_one "gitlab.com/montray/grpcex/gitlab.com/montray/sharedproto/ms-one"
	"google.golang.org/grpc"
)

type server struct {
	ms_one.UnimplementedMsOneServer
}

func (s *server) Add(ctx context.Context, req *ms_one.AddRequest) (*ms_one.AddResponse, error) {
	log.Println("received message", req.ValueOne, req.ValueTwo)
	return &ms_one.AddResponse{Value: req.ValueOne + req.ValueTwo}, nil
}

func main() {
	lis, err := net.Listen("tcp", fmt.Sprintf(":8081"))
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	s := grpc.NewServer()
	ms_one.RegisterMsOneServer(s, &server{})
	log.Printf("server listening at %v", lis.Addr())
	if err := s.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}
